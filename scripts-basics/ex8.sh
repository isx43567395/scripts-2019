#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
#8. Fer un programa que rep com a argument noms d̉usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
#---------------------------------------------------------
OK=0
for user in $*
do
  egrep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
   echo $user
  else
   echo $user >> /dev/stderr
  fi
done
exit $OK
