#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# 7) Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
#    Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
#    Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
# b) Ampliar amb el cas: prog -h|--help.
#----------------------------------
OK=0 # Tot bé
ERR_NARGS=1 # Codi d'error nº arguments incorrecte
ERR_TIPUS=2 # Codi d'error per si l argument no és correcte

if [ $1 = "-h" -o $1 = "--help" ]
then
  echo "AJUDA: -f per a avaluar fitxers, -d per a avaluar directoris"
  echo "Ha d'haver-hi mínim 5 arguments"
  echo "Usage: $0 [-f | -d] arg1 arg2 arg3 arg4"
  exit $OK
fi

if [ $# -ne 5 ]
then
  echo "Ha d'haver-hi 5 arguments"
  echo "Usage: $0 [-f | -d] arg1 arg2 arg3 arg4"
  exit $ERR_NARGS
fi

# Nomes queden les variables -f i -d per a avaluar
if [ "$1" != "-f" -a "$1" != "-d" ]
then
  echo "ERROR format arguments"
  echo "Usage: $0 [-f | -d] arg1 arg2 arg3 arg4"
  exit $ERR_NARGS
fi

# XIXA

llista=$(echo $* | cut -d" " -f2-)

for element in $llista
do
  if ! [ $1 $element ]; then
    OK=$ERR_TIPUS
  fi
done
exit $OK

