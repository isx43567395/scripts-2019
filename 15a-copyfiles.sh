#! /bin/bash
# Alejandro López Herrador
# 10/02/2020
# Descripció: Validar file origen (regular file)
#			  Validar dir destí
#			  Validar num args (mim 2)
#             Fer un copy
# --------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
OK=0
file=$1
dir=$2
# Validar arguments, si no es correcte, plegar
if [ $# -ne 2 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 file origen dir destí"
  exit $ERR_NARGS
fi
#Validar dir destí
if ! [ -d $dir ]; then
  echo "ERROR: $dir no és un directori"
  echo "USAGE: $0 file(regular file) dir-destí"
  exit $ERR_NODIR
fi
#Validar file origen (regular file)
if ! [ -f $file ]; then
  echo "ERROR: $file no és un regular file"
  echo "USAGE: $0 file(regular file) dir-destí"
  exit $ERR_NOFILE
fi
#xixa
cp $file $dir
exit $OK
 
