#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
#10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
#---------------------------------------------------------
OK=0
MAX=$1
cont=1
read -r line
while [ $cont -le $MAX ]
do
  echo "$cont: $line"
  cont=$((cont+1))
  read -r line
done
exit $OK
