#! /bin/bash
# Alejandro López Herrador
# 04/02/2020
# dia-mes
# -------------------------
# Si num arg no es correcte, plegar
ERR_NARGS=1
ERR_MIVALID=2
OK=0
if [ $# -ne 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 dir"
  exit $ERR_NARGS
fi
#HELP
if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo "Alejandro López"
  echo "Curs ASIX 2019-2020"
  echo "USAGE: $0 mes"
  exit $OK
fi
# Validar, mes valid, si no es del 1-12 pleguem
if ! [ $1 -ge 1 -a $1 -le 12 ]; then
  echo "ERROR: $1 valor invalid "
  echo "USAGE: $0 num[1-12]"
  exit $ERR_MIVALID
fi
#xixa: Determinar numero de dies
mes=$1
case $mes in 
  2)
   echo "Mes $mes tiene 28 dias"
   ;;
  4|6|9|11)
   echo "Mes $mes tiene 30 dias"
   ;;
  *)
   echo "Mes $mes tiene 31 dias"
   ;;
esac
exit $OK
