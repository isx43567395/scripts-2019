#! /bin/bash
# Alejandro López Herrador
# 18/02/2020
# Descripció: 
# 5. Mostrar línia a línia l̉entrada estàndard, retallant només els primers 50 caràcters.
# --------------------------------
OK=0
while read -r line
do
  echo $line | cut -c1-50
done
exit $OK
