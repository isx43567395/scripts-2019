#! /bin/bash
# Alejandro López Herrador
# 03/02/2020
# Descripció: processar nota a nota i 
#   indicar si suspès, aprovat, notable o excel.lent
# $ prog nota[...]
# ------------------------
ERR_NARGS=1
OK=0
# Validar argument, si no es correcte, plegar
if [ $# -lt 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 nota o notes"
  exit $ERR_NARGS
fi

llistaNotes=$*
for nota in $llistaNotes
do
  # Validar nota
  if ! [ $nota -ge 0 -a  $nota -le 10 ]; then
    echo "ERROR: la nota $nota no és vàlida" >> /dev/stderr
    echo "Valors vàlids: 0 - 10" >> /dev/stderr    
  elif [ $nota -lt 5 ]; then
    echo "$nota: Suspès"
  elif [ $nota -lt 7 ]; then
    echo "$nota: Aprovat"
  elif [ $nota -lt 9 ]; then
    echo "$nota: Notable"
  else
    echo "$nota: Excel.lent"
  fi
done
exit $OK
