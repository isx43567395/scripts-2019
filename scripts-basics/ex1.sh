#! /bin/bash
# Alejandro López Herrador
# 18/02/2020
# Descripció: 
# 1. Mostrar l̉entrada estàandard numerant línia a línia
# --------------------------------
OK=0
cont=0
while read -r line
do
  cont=$((cont+1))
  echo "$cont: $line" 
done
exit $OK
