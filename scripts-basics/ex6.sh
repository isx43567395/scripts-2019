#! /bin/bash
# Alejandro López Herrador
# 18/02/2020
# Descripció: 
#6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra 
#quants dies eren laborables i quants festius. Si l’argument no és un dia de la 
#setmana genera un error per stderr.
#Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday# --------------------------------
#---------------------------------------------
OK=0
laborables=0
festivos=0
for arg in $*
do
 case $arg in
   dilluns|dimarts|dimecres|dijous|divendres)
    laborables=$((laborables +1))
    ;;
   dissabte|diumenge)
    festivos=$((festivos +1))
    ;;
   *)
    echo "Error: $arg No es un dia de la setmana" >> /dev/stderr
 esac	
done
#Muestra el resultado
echo "Dias laborals: $laborables"
echo "Dias festivos: $festivos"
exit $OK
