#! /bin/bash
# Alejandro López Herrador
# 03/03/2020
# Examen 08 
#----------------------------------------------------
ERR_NARGS=1 #Codigo error de argumentos
ERR_DIR=2 #Codigo error no existe el directorio.
status=0
#Mostrar ayuda.
if [ "$1" = "--help" ]; then
  echo "AYUDA: $0 dirDesti xfile[...] "
  echo "Tiene que haber minimo dos argumentos"
  exit $status
fi
#Validación de argumentos
if [ $# -lt 2 ]; then
  echo "Tiene que haber minimo dos argumentos"
  echo "Usage: $0 dirDesti xfile[..]"
  exit $ERR_NARGS
fi
#Comprobamos que exista el directorio destino
dirDesti=$1
if [ ! -d $dirDesti ]; then
  echo "ERROR: El directorio $dirDesti, no existe"
  exit $ERR_DIR
fi
#xixa
cont_err=0
llista=$(echo $* | cut -d" " -f2-)
for file in $llista
do
  if [ ! -e $file ]; then
   echo "Error: el fichero $file, no existe" >> /dev/stderr
   status=3
  fi
  validaVegenere $file
  if [ $? -eq 0 ]; then
    cp $file $dirDesti
    echo "$file" 
  else
   echo "$file" >> /dev/stderr
   cont_err=$((cont_err+1)) 
   status=4
  fi
done
echo "Cantidad total de ficheros que no son cifrados utilizando el codigo vigenere: $cont_err"
exit $status










