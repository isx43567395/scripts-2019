#! /bin/bash
# Alejandro López Herrador
# 04/02/2020
# Exemple case
# -------------------------
# dl dt dc dj dv ds dm
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
     echo "$1 és un dia laborable"
   ;;
  "ds"|"dm")
     echo "$1 és un dia festiu"
     ;;
  *)
   echo "aixo $1 no es un dia"
   ;;
esac
exit 0
case $1 in
  [aeiou])
    echo "$1 Es una vocal"
  ;;
  [bcdfghjklmnpqrstvwxyz])
   echo "$1 Es una consonant"
   ;;
  *)
   echo "$1 és una altra cosa"
   ;;
esac

exit 0

case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "és una nena"
    ;;
  *)
   echo "indefinit"
   ;;
esac
exit 0

