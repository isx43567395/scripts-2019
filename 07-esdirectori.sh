#! /bin/bash
# Alejandro López Herrador
# 03/02/2020
# Exercici mostrar directori si es
#       $ prog dir
# ------------------------
# Si num arg no es correcte, plegar
if [ $# -ne 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 dir"
  exit 1
fi
# Validar si no es un directori
if ! [ -d $1 ]; then
  echo "ERROR: $1 no es un directori"
  echo "Usage: $0 dir"
  exit 2
fi
# xixa
dir=$1
ls $dir
exit 0
