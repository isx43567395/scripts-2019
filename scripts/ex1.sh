#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
# 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
# --------------------------------
OK=0
for arg in $*
do
  linea=$(echo "$arg" | wc -c)
  if [ $linea -ge 4 ]; then
    echo $arg
  fi
done
exit $OK
