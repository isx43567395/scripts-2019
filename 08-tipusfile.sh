#! /bin/bash
# Alejandro López Herrador
# 04/02/2020
# Exercici per saber si es un regular file, dir o link
#       $ prog /regular/dir/file
# ------------------------
# Si num arg no es correcte, plegar
ERR_NARGS=1
if [ $# -ne 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 fit"
  exit $ERR_NARGS
fi
#xixa
fit=$1
if ! [ -e $fit ]; then
  echo "ERROR: $fit No existeix"
elif  [ -h $fit ]; then
  echo "$fit és un symbolic lynk"
elif [ -f $fit ]; then
  echo "$fit és un regular file"
elif [ -d $fit ]; then
  echo "$fit és un directori"
else
  echo "$fit és una altra cosa"
fi
exit 0

