#! /bin/bash
# Alejandro López Herrador
# 17/02/2020
# Descripció: $0 [-a -b -c -d -e -f] args[...]
# --------------------------------
ERR_NARGS=1
OK=0
# Validar arguments, si no es correcte, plegar
if [ $# -lt 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 [-a -b -c -d -e -f] args[..]"
  exit $ERR_NARGS
fi
opcions=""
arguments=""
for arg in $*
do
  case $arg in 
    -a|-b|-c|-d|-e|-f)
      opcions="$opcions $arg"
      ;;
    *)
     arguments="$arguments $arg"
     ;;
  esac
done
echo "Les opcions son: $opcions"
echo "Els arguments son: $arguments"
exit $OK
