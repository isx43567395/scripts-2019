#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 09-03-2020
# Funcions 
# -----------------

function hola(){
  echo "hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){

  echo $(($1+$2))
  return 0
}

# showUser(login)
#Mostrar un a un els camps amb label
function showUser(){
ERR_NARGS=1
ERR_NOLOGIN=1
OK=0
#1) Validar que rep 1 arg
  if [ $# -ne 1 ]; then
    echo "ERROR: Número de argumentos incorrecto"
    echo "USAGE: $0 arg"
    return $ERR_NARGS
  fi
#2) validar existeix login
login=$1
line=""
line=$(egrep "^$login:" /etc/passwd 2> /dev/null)
if [ -z "$line" ]; then
  echo "El login $1 no existe"
  return $ERR_NOLOGIN
fi
#xixa
login=$(echo $line | cut -d: -f1)
uid=$(echo $line | cut -d: -f3)
gid=$(echo $line | cut -d: -f4)
gecos=$(echo $line | cut -d: -f5)
home=$(echo $line | cut -d: -f6)
shell=$(echo $line | cut -d: -f7)
gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

echo "login: $login"
echo "uid: $uid"
echo "gid: $gid"
echo "gecos: $gecos"
echo "home: $home"
echo "shell: $shell"
echo "gname: $gname"
return 0
}

#showUserGecos(login)
function showUserGecos(){
ERR_NARGS=1
ERR_NOLOGIN=1
OK=0
#1) Validar que rep 1 arg
if [ $# -ne 1 ]; then
  echo "ERROR: Número de argumentos incorrecto"
  echo "USAGE: $0 arg"
  return $ERR_NARGS
fi
#2) validar existeix login
login=$1
line=""
line=$(egrep "^$login:" /etc/passwd 2> /dev/null)
if [ -z "$line" ]; then
echo "El login $1 no existe"
return $ERR_NOLOGIN
fi
#xixa
gecos=$(echo $line | cut -d: -f5)
name=$(echo $gecos | cut -d, -f1)
office=$(echo $gecos | cut -d, -f2)
office_phone=$(echo $gecos | cut -d, -f3)
home_phone=$(echo $gecos | cut -d, -f4)
echo "name: $name"
echo "office: $office"
echo "office_phone: $office_phone"
echo "home_phone: $home_phone"
return 0
}

#showGroup(gnome)
function showGnome(){
ERR_NARGS=1
ERR_NOGNOME=1
OK=0
#1) Validar que rep 1 arg
if [ $# -ne 1 ]; then
  echo "ERROR: Número de argumentos incorrecto"
  echo "USAGE: $0 arg"
  return $ERR_NARGS
fi
#2) validar existeix gnome
gnome=$1
line=""
line=$(egrep "^$gnome:" /etc/group 2> /dev/null)
if [ -z "$line" ]; then
  echo "El grup $1 no existe"
  return $ERR_NOGNOME
fi
#xixa
gid=$(echo $line | cut -d: -f3)
users=$(echo $line | cut -d: -f4)
echo "group: $gnome"
echo "gid: $gid"
echo "users: $users"
return 0
}


# showUserList(login[...])
function showUserList(){
ERR_NARGS=1
ERR_NOLOGIN=2
OK=0
#1) Validar que rep 1 arg
if [ $# -lt 1 ]; then
  echo "ERROR: Número de argumentos incorrecto"
  echo "USAGE: $0 login[...]"
  return $ERR_NARGS
fi
#xixa
for login in $*
do
  line=$(egrep "^$login:" /etc/passwd 2> /dev/null)
  if [ -z "$line" ]; then
    echo "El login $login no existe" >> /dev/stderr
    OK=$ERR_NOLOGIN
 else
  login=$(echo $line | cut -d: -f1)
  uid=$(echo $line | cut -d: -f3)
  gid=$(echo $line | cut -d: -f4)
  gecos=$(echo $line | cut -d: -f5)
  home=$(echo $line | cut -d: -f6)
  shell=$(echo $line | cut -d: -f7)
  gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

  echo "login: $login"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "gecos: $gecos"
  echo "home: $home"
  echo "shell: $shell"
  echo "gname: $gname"
 fi
done
return $OK
}

# showUserIn < filein
function showUserIn (){
while read -r line
do
  linea=$(egrep "^$line:" /etc/passwd 2> /dev/null)
  if [ -z "$linea" ]; then
   echo "El login $line no existe" >> /dev/stderr
   OK=$ERR_NOLOGIN
  else
    login=$(echo $linea | cut -d: -f1)
    uid=$(echo $linea | cut -d: -f3)
    gid=$(echo $linea | cut -d: -f4)
    gecos=$(echo $linea | cut -d: -f5)
    home=$(echo $linea | cut -d: -f6)
    shell=$(echo $linea | cut -d: -f7)
    gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

    echo "login: $login"
    echo "uid: $uid"
    echo "gid: $gid" 
    echo "gecos: $gecos"
    echo "home: $home"
    echo "shell: $shell"
    echo "gname: $gname"
  fi
done
}

#showGroupMainMembers gnome
showGroupMainMembers(){
ERR_NARGS=1
ERR_NOGNOME=2
OK=0
#1) Validar que rep 1 arg
if [ $# -ne 1 ]; then
  echo "ERROR: Número de argumentos incorrecto"
  echo "USAGE: $0 gnome"
  return $ERR_NARGS
fi
gname=$1
gid=""
gid=$(egrep "^$gname:" /etc/group | cut -d: -f3)
if [ $? -ne 0 ]; then
  echo"Error: El gnome $gname, no existe"
  exit $ERR_NOGNOME  
fi
echo "Llistat grup: $gname($gid)"
egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6,7 | sed 's/:/  /g' | sort -t' ' -k2g,2 | tr '[:lower:]' '[:upper:]' | sed -r 's/^([^ ]*)/\t\1/g'
}

#showAllShells llistat complet
showAllShells(){
shells=$(cut -d: -f7 /etc/passwd | sort -u)
for shell in $shells
do
  echo $shelli:
  egrep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6 | sort -t: -k1g,1 | sed 's/:/ /g' | sed -r 's/([^:]*)/\t\1/g'

done
}


#showAllShells2 llistat complet
showAllShells2(){
shells=$(cut -d: -f7 /etc/passwd | sort -u)
for shell in $shells
do
  cont_users=$(egrep ":$shell$" /etc/passwd | cut -d: -f1 | wc -l)
  if [ $cont_users -ge 2 ];then
  echo "$shell($cont_users)"
	egrep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6 | sort -t: -k1g,1 | sed 's/:/ /g' | sed -r 's/([^:]*)/\t\1/g'
  fi
done
}

