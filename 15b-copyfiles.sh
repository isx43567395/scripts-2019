#! /bin/bash
# Alejandro López Herrador
# 10/02/2020
# Descripció: Validar file origen (regular file)
#			  Validar dir destí
#			  Validar num args (mim 2)
#             Fer un copy
# --------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
OK=0
# Validar arguments, si no es correcte, plegar
if [ $# -lt 2 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 file[...]  dir-destí"
  exit $ERR_NARGS
fi
desti=$(echo $* | sed 's/^.* //' )
llistafiles=$(echo $* | sed 's/ [^ ]*$//')
#Validar dir destí
if ! [ -d $desti ]; then
  echo "ERROR: $desti no és un directori"
  echo "USAGE: $0 file(regular file) dir-destí"
  exit $ERR_NODIR
fi
#xixa
for file in $llistafiles
do
  if ! [ -f $file ]; then
    echo "ERROR: $file no és un regular file" >> /dev/stderr
    echo "USAGE: $0 file(regular file) dir-destí" >> /dev/stderr
  fi
  cp $file $desti
done
exit $OK
 
