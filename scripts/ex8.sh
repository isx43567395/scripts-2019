#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
#8) Programa: prog file…
# a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o un missatge d̉error per stderror si no s̉ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
# Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
# b) Ampliar amb el cas: prog -h|--help.
#----------------------------------
OK=0 # Tot bé
ERR_NARGS=1 # Codi d'error nº arguments incorrecte
ERR_FILE=2 # Codi d'error per si l'argument no és correcte

if [ $1 = "-h" -o $1 = "--help" ]
then
  echo "AJUDA: minim un file"
  echo "Usage: $0 file..."
  exit $OK
fi
#Validació d'arguments
if [ $# -lt 1 ]
then
  echo "ERROR: Minim un file"
  echo "Usage: prog file..."
  exit $ERR_NARGS
fi
#xixa
cont=0
for file in $*
do
  comprimit=$(gzip $file &> /dev/null)
  if [ $? -eq 0 ]; then
    echo "$file: s'ha comprimit correctament."
    cont=$((cont+1))
  else
    echo "Error: $file no s'ha pogut comprimir."
    OK=$ERR_FILE
  fi    
done
echo "Files comprimits: $cont"
exit $OK
