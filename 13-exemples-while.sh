#! /bin/bash
# Alejandro López Herrador
# 05/02/2020
# Descripció: Exemples bucle while
# --------------------------------

# 9) Mostrar en majúscules stdin
#   i numerant les línies
cont=1
while read -r line
do
 echo "$cont: $line" | tr '[:lower:]' '[:upper:]'
 cont=$((cont+1))
done
exit 0

# 8) Processar stdin fins a token "FI"
read -r line
while [ $line != "FI" ]
do
  echo $line
  read -r line
done
exit 0
#Programa que processa la entrada estandard linea a linea i mostran el numero de línea

cont=1
while read -r line
do
  echo "$cont: $line"
  cont=$((cont+1)) 
done
exit 0

# 6) Mostrar la entrada estandard línea a línea
while read -r line
do
  echo $line
done
exit 0

#Lo mismo que el anterior pero numerando las lineas
cont=1
while [ -n "$1" ]
do
  echo "$cont: $1, $#, $* "
  cont=$((cont+1))
  shift
done
exit 0

#Mientras hay xixa en el $1, muestra y desplaza
while [ -n "$1" ]
do
   echo "$1 $#: $*"
   shift
done
exit 0

#Mostrar els arguments
while [ $# -gt 0 ]
do
  echo "$#: $*"
  shift
done 
exit 0

#comptador decrexeint del arg rebut
MIN=0
cont=$1
while [ $cont -ge $MIN ]
do
   echo "$cont"
   cont=$((cont-1))
done
exit 0

# Mostrar del [1-10]
cont=1
MAX=10
while [ $cont -le $MAX ]
do
  echo "$cont"
  cont=$((cont+1))
done
exit 0
