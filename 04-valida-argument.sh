#! /bin/bash
# Alejandro López Herrador
# 03/02/2020
# Valida arguments
#       $ prog arg1 arg2
# Validar la synopsis i mostra els arguments
#------------------------------------------
#Si el num d'args no es correcte, pleguem
if [ $# -ne 2 ]; then
   echo "ERROR: N# Args incorrectes"
   echo "USAGE: $0 arg1 arg2"
   exit 1
fi
# xixa 
echo "Els dos arguments son: $1, $2"
exit 0
