#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# 6) Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
# --------------------------------
OK=0
while read -r line
do
 nombre=$(echo $line | cut -c1)
 apellido=$(echo $line | cut -d' ' -f2)
 echo "$nombre. $apellido"  	
done
exit $OK

#echo "Tom Snyder" | sed -r 's/^(.)[^ ]* (.*)/\1. \2./'

