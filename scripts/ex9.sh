#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
#9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#  Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
#  No cal validar ni mostrar res!
#  Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#  retona:     opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»
#--------------------------------
OK=0 # Tot bé
opcions=""
cognom=""
edat=""
arguments=""
while [ -n "$1" ]
do
  case $1 in
     -r|-m|-j)
	opcions="$opcions $1"
	;;
     -c)
      cognom=$2
      shift
      ;;
     -e)
      edat=$2
      shift
      ;;
     *)
     arguments="$arguments $1"
     ;;
  esac
  shift
done
echo "opcions: $opcions"
echo "cognom: $cognom"
echo "edat: $edat"
echo "arguments: $arguments"
exit $OK
