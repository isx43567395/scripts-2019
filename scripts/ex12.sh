#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# 12) Programa -h uid…
#    Per a cada uid mostra la informació de l̉usuari en format:
#    logid(uid) gname home shell
#----------------------------------------------------
OK=0 # Tot bé
ERR_NARGS=1 #Codi d'error d'arguments
if [ "$1" = "-h" ]; then
  echo "AJUDA: $0 i uid.."
  echo "Ha d'haver-hi mínim 1 argument"
  echo "Usage: $0 -h uid..."
  exit $OK
  fi
if [ $# -lt 1 ]; then
  echo "Ha d'haver-hi com a minim 1 argument"
  echo "Usage: $0 uid..."
  exit $ERR_NARGS
fi
#xixa
for uids in $*
do
  linea=$(egrep "^[^:]*:[^:]*:$uids:" /etc/passwd)
  if [ $? -eq 0 ];then
    login=$(echo "$linea" | cut -d: -f1)
    uid=$(echo "$linea" | cut -d: -f3)
    gid=$(echo "$linea" | cut -d: -f4)
    gname=$( egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    home=$(echo "$linea"  | cut -d: -f6)
    shell=$(echo "$linea" | cut -d: -f7)
    echo "$login($uid) $gname $home $shell"
  else
    echo "Error: el uid $uids es inexistent"
  fi
done
exit $OK
