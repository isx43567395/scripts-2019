#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
#7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
#------------------------------------------------------------------------------------------------------
OK=0
while read -r line
do
  linea=$(echo $line | wc -c)
  if [ $linea -gt 60 ]; then
   echo $line
  fi
done
exit $OK
