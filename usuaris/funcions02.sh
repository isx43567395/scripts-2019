#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 12-03-2020
# Funcions
# -----------------

#funcion para contar la entrada estandar.
function contStdin(){
  cont=1
  while read -r line
  do
    echo "$cont: $line"
   cont=$((cont+1))
  done
}

function numFile(){
  ERR_NARGS=1
  ERR_NOFILE=2
  OK=0
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 regularFile"
    return $ERR_NARGS
  fi
  if [ ! -f "$1" ]; then
    echo "ERROR: $file no es un regular file"
    echo "USAGE: $0 regularFile"
    return $ERR_NOFILE
  fi
  fileIn=$1
  cont=1
  while read -r line
  do
    echo "$cont: $line"
    cont=$((cont+1))
  done < $fileIn
}

# numfileDefault [file]
function numFileDefault(){
  ERR_NARGS=1
  ERR_NOFILE=2
  OK=0
  if [ $# -gt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 regularFile"
    return $ERR_NARGS
  fi
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    if [ ! -f "$1" ]; then
      echo "ERROR: $file no es un regular file"
      echo "USAGE: $0 regularFile"
      return $ERR_NOFILE
    fi

    fileIn=$1
  fi
  cont=1
  while read -r line
  do
    echo "$cont: $line"
    cont=$((cont+1))
  done < $fileIn
  return 0
}

# filterGid file
# Mostar els usuaris de gi >=500
function filterGid(){
  ERR_NARGS=1
  ERR_NOFILE=2
  OK=0
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 file"
    return $ERR_NARGS
  fi
  if [ ! -f "$1" ]; then
    echo "ERROR: $file no es un regular file"
    echo "USAGE: $0 regularFile"
    return $ERR_NOFILE
  fi
  fileIn=$1
  while read -r line
  do
    gid=$(echo $line | cut -d: -f4)
    if [ $gid -ge 500 ]; then 
      echo $line | cut -d: -f1,3,4,6
    fi 

  done < $fileIn
  return 0 
}


# filterGidDefault file
# Mostar els usuaris de gi >=500
function filterGidDefault(){
  ERR_NARGS=1
  ERR_NOFILE=2
  OK=0
  if [ $# -gt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 file"
    return $ERR_NARGS
  fi
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    if [ ! -f "$1" ]; then
      echo "ERROR: $file no es un regular file"
      echo "USAGE: $0 regularFile"
      return $ERR_NOFILE
    fi
    fileIn=$1
  fi
  while read -r line
  do
    gid=$(echo $line | cut -d: -f4)
    if [ $gid -ge 500 ]; then
      echo $line | cut -d: -f1,3,4,6
    fi
  done < $fileIn
  return 0
}

