#! /bin/bash
# Alejandro López Herrador
# 17/02/2020
# Descripció: $0 -a file -b -c -d edat -e arg[...]
# --------------------------------
ERR_NARGS=1
OK=0
# Validar arguments, si no es correcte, plegar
if [ $# -lt 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 -a file -b -c -d edat -e arg[..]"
  exit $ERR_NARGS
fi
opcions=""
arguments=""
fitxer=""
num=""
while [ -n "$1" ]
do
  case $1 in
    -a)
      opcions="$opcions $1"
      fitxer=$2
      shift     
      ;; 
    -d)
      opcions="$opcions $1"
      num=$2
      shift     
      ;;   
    -b|-c|-e)
       opcions="$opcions $1"
       ;;
    *)
      arguments="$arguments $1"
      ;;
  esac
  shift
done
echo "Les opcions son: $opcions"
echo "Els arguments son: $arguments"
echo "Els fitxers son: $fitxer"
echo "Els numeros son: $num"
exit $OK
