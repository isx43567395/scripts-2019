#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
# 3) Processar arguments que són matricules:
# a) llistar les vàlides, del tipus: 9999-AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d̉errors (de no vàlides)
# --------------------------------
status=0
for mat in $*
do
  echo "$mat" | egrep "^[0-9]{4}-[A-Z]{3}$" 2> /dev/null
 if [ ! $? -eq 0 ]; then
   echo "ERROR: $mat matricula no valida" >> /dev/stderr
   status=$((status+1))
 fi 
done
exit $status
