#! /bin/bash
# Alejandro López Herrador
# 18/02/2020
# Descripció: 
# 3. Fer un comptador des de zero fins al valor indicat per l̉argument rebut.
# --------------------------------
OK=0
cont=0
MAX="$1"
while [ $cont -le $MAX ]
do
  echo $cont
  cont=$((cont+1))
done
exit $OK
