#! /bin/bash
# Alejandro López Herrador
# 03/02/2020
# validar nota
#       $ prog edat
# ------------------------
# Validar argument, si no es correcte, plegar
if [ $# -ne 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 nota"
  exit 1
fi
# Validar nota 
if ! [ $1 -ge 0 -a  $1 -le 10 ]; then
  echo "ERROR: nota $1 no valida [0-10]"
  echo "USAGE: $0 nota"
  exit 2
fi
# xixa
nota=$1
if [ $nota -lt 5 ]; then
  msg="Suspès"
else
  msg="Aprovat"
fi
echo $msg
exit 0
