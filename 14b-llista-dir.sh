#! /bin/bash
# Alejandro López Herrador
# 10/02/2020
# Descripció: Validar que es rep un argument i que és un directori i llistar-ne el contingut.
#             Per llistar el contingut amb un simple ls ja n'hi ha prou.
# --------------------------------
ERR_NARGS=1
ERR_NODIR=2
OK=0
# Validar argument, si no es correcte, plegar
if [ $# -ne 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 dir"
  exit $ERR_NARGS
fi
dir=$1
if ! [ -d $dir ]; then
  echo "ERROR: $dir no es un directori"
  echo "USAGE: $0 dir"
  exit $ERR_NODIR
fi
#xixa
filelist=$(ls $dir)
cont=1
for file in $filelist
do
  echo $cont: $file
  cont=$((cont+1))
done
exit $OK
