#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
#9. Fer un programa que rep per stdin noms d̉usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.:
#---------------------------------------------------------
OK=0
while read -r line
do
  egrep "^$line:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
   echo $line
  else
   echo $line >> /dev/stderr
  fi
done
exit $OK
