#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# 5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
# --------------------------------
OK=0
while read -r line
do
 linea=$(echo $line | wc -c) 
 if [ $linea -lt 50 ]; then
  echo $line
 fi 	
done
exit $OK
