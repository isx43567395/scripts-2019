#! /bin/bash
# Alejandro López Herrador
# 17/02/2020
# Descripció: $0 -a file -b -c -d edat -e arg[...]
# --------------------------------
ERR_NARGS=1
ERR_MKDIR=2
status=0
# Validar arguments, si no es correcte, plegar
if [ $# -lt 1 ]; then
  echo "ERROR: N# Args incorrectes"
  echo "USAGE: $0 nomdir[...]"
  exit $ERR_NARGS
fi
for dir in $*
do
  mkdir $dir &> /dev/null
 if [ $? -ne 0 ]; then
   echo "ERROR: No s'ha pogut crear $dir" >> /dev/stderr
   status=$ERR_MKDIR
 fi 
done	
exit $status
