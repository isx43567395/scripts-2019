#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# 4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..
# --------------------------------
OK=0
cont=1
while read -r line
do
  mayus=$(echo $line | tr '[:lower:]' '[:upper:]')
  echo "$cont: $mayus"
  cont=$((cont+1))	
done
exit $OK
