#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# Programa: prog.sh
# Rep per els gids com a arguments de la línia d'ordres i llista per stdout la informació de cada un d̉aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#----------------------------------------------------
OK=0 # Tot bé
for gid in $*
do
 linea=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group)
 if [ $? -eq 0 ]; then

   gname=$(echo $linea | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
   gid=$(echo $linea | cut -d: -f3)
   users=$( echo $linea | cut -d: -f4 | tr '[:lower:]' '[:upper:]')
   echo "gname: $gname, gid: $gid, users: $users"
 else
  echo "Error: el gid $gid es inexistent" >> /dev/stderr
 fi
done
exit $OK
