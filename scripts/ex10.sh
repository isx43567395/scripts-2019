#! /bin/bash
# Alejandro López Herrador
# 24/02/2020
# Descripció: 
# 10) Programa: prog.sh
# Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#--------------------------------
OK=0 # Tot bé
while read -r line
do
 linea=$(egrep "^[^:]*:[^:]*:$line:" /etc/group)
 if [ $? -eq 0 ]; then

   gname=$(echo $linea | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
   gid=$(echo $linea | cut -d: -f3)
   users=$( echo $linea | cut -d: -f4 | tr '[:lower:]' '[:upper:]')
   echo "gname: $gname, gid: $gid, users: $users"
 else
  echo "Error: el gid $line es inexistent" >> /dev/stderr
 fi
done
exit $OK
