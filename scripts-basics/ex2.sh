#! /bin/bash
# Alejandro López Herrador
# 18/02/2020
# Descripció: 
# 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
# --------------------------------
OK=0
cont=0
for arg in $*
do
  cont=$((cont+1))
  echo "$cont: $arg"
done
exit $OK
