#! /bin/bash
# Alejandro López Herrador
# 19/02/2020
# Descripció: 
# 2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters.
# --------------------------------
OK=0
cont=0
for arg in $*
do
   caracters=$(echo "$arg" | wc -c)
  if [ $caracters -ge 3 ]; then
   cont=$((cont+1))
  fi
done
echo "Hay $cont argumentos con 3 o más caracters"
exit $OK
