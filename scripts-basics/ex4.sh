#! /bin/bash
# Alejandro López Herrador
# 18/02/2020
# Descripció: 
# 4. Fer un programa que rep com a arguments números de mes (un o mes) i indica per a cada mes rebut quants dies té el més.
# --------------------------------
OK=0
for mes in $*
do
 case $mes in
   4|6|9|11)
    echo "$mes: Tiene 30 dias"
    ;;
   1|3|5|7|8|10|12)
    echo "$mes: Tiene 31 dias"
    ;;
   *)
    echo "$mes: Tiene 28 dias"
 esac	
done
exit $OK
